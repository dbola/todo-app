// questa funzione mi aggiunge todo dentro il Database usando lumen
async function addTodo(todoDescription) {
    // faccio la chiamata http POST che simula quello che fa postman
    var newTodo={
        description: todoDescription
    }
    await fetch('http://localhost:8080/api/todos', {
        method: 'POST', 
        headers:{'Content-Type':'application/json'},
        body: newTodo
    })
}


// da qui parte la nostra app JS
// la parol async dice al browser che all'interno di questa funzione ce un'operazione asincrona(await)
async function main() {
    console.log('running main');
    // prendo il tag div con
    var divApp = document.getElementById("app");
    // creo il tag title
    var title= document.createElement('h1');
    title.textContent="TODO APP";
    // aggiungo il tag dentro div
    divApp.appendChild(title);
    // prendo i todos via HTTP con la funzione fetch
    //N.B la parola await permette di rendere sincrona un'operazione asincrona
    var todosResponse = await fetch('http://localhost:8080/api/todos');
    var todosJSon=await todosResponse.json();
    console.log(todosJSon);

    // campo per inserire nuovo todo ----------------------------------
    var textInput=document.createElement('input');
    textInput.placeholder = 'inserisci un nuovo todo';
    //aggiungo un id all'input perche mi serve dopo per essere recuperato
    textInput.id='addTodoInput';
    divApp.appendChild(textInput);

    // bottone per aggiungere ----------------------------------------
    var addButton = document.createElement('button');
    addButton.textContent='Aggiungimi';
    divApp.appendChild(addButton);

    // attach l'evento click del button ----------------------
    addButton.addEventListener('click', function() {
        // recupero la descrizione del nuovo todo
        var todoDesc= document.getElementById('addTodoInput').value;
        // uso la funzione addTodo che definisco sopra
        addTodo(todoDesc);
    })

    //creo la lista Ul in JS -------------------------
    var ulList= document.createElement('ul');
    //aggiungpo la lista alla fine del container div#app
    divApp.appendChild(ulList);

    // itero gli elementi e li aggiungo all'UL
    for(var i = 0; i < todosJSon.length; i++ ) {
        //creo il tag LI
        var liElement= document.createElement('li');
        //creo una variabile con l'elemento i-esimo della lista(il singolo todo)
        var todo=todosJSon[i];
        // metto la descrizione come contenuto del LI
        liElement.textContent = '#' + todo.id + ' ' + todo.description;
        liElement.textContent+= ' (';
        if(todo.done) {
            liElement.textContent+='completato';
        }
        else {
            liElement.textContent+='Da completare';
        }
        liElement.textContent+=')';
        // lo appendiamo alla lista
        ulList.appendChild(liElement);
    }

}
// assoccio l'evento DOMContentLoader all funzione main
document.addEventListener("DOMContentLoaded", function() {
    main();
});