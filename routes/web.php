<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/api/info', function () use ($router) {  // /api/info è un url o route
    return "v 0.0.1";
});


// GET (select)
// quando arriva una chiamata in GET su url /api/todos
$router->get('/api/todos', function () use ($router) {  // /api/info è un url o route
    // carico l'array di todo: collegare al database (in file .env), fare la select e restituire il risultato
    // connessione
    $result = app('db')->select("SELECT * FROM tasks"); // sceglie tutto da tasks
    return $result;
});

// INSERT (post)
//quando arriva una chiamata in POST su url /api/todos 
$router->post('/api/todos', 'TodoController@add'); // chiama il metodo add del Controller Todo   (si crea in app->http->controllers )


// UPDATE (put)
// quando arriva una chiamata in PUT su url /api/todos
//N.B il parametro {id} verra passato come parametyro al metodo update coi aggiorno elemento
$router->put('/api/todos/{id}', 'TodoController@update'); // chiama il metodo update del Controller Todo   (si crea in app->http->controllers )

// DELETE (delete)
// quando arriva una chiamata in delete su url /api/todos
//N.B il parametro {id} verra passato come parametyro al metodo update coi aggiorno elemento
$router->delete('/api/todos/{id}', 'TodoController@delete'); // chiama il metodo update del Controller Todo   (si crea in app->http->controllers )

