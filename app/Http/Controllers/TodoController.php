<?php

namespace App\Http\Controllers;

// questo serve per usare l'oggetto Request nei metodi
use Illuminate\Http\Request;
// questo serve per usare l'oggetto Response nei metodi
use Illuminate\Http\Response;


class TodoController extends Controller
{

    //      ADD      
    // aggiungere nuovo todo
    // questo viene chiamato dalla route TodoController@add
    public function add(Request $request)
    {
        $description = $request->input('description');
        $results = app('db')->insert(
            "insert into tasks(description,done,insertDate) values('$description',false, now())"
        );
        // ritorno 201 created
        return new Response(null, 201);
    }


    //       UPDATE
    // aggiorna il todo con id passato
    public function update(Request $request, $id)
    {
        $description = $request->input('description');
        $done = $request->input('done');
        $results = app('db')->update(
            //"UPDATE tasks SET desciption='nuovo todo', done='false' WHERE id=1"
            "UPDATE tasks SET description='$description', done=$done WHERE id=$id"
        );
        // ritonro 200 ok
        return new Response(null, 200);
    }

    public function delete(Request $request, $id)
    {
        $results = app('db')->delete(
            "DELETE FROM tasks WHERE id=$id"
        );
        // ritonro 204 no content
        return new Response(null, 204);
    }

}
